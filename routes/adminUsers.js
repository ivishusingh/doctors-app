var express = require('express');
var router = express.Router();
const tokenValidator = require('../utils/token/validator');
const isAdminValidator = require('../utils/token/adminValidator');
const adminValidator = require('../middlewares/validators/auth.js');
const adminController = require('../controllers/adminUsers');
const dashboardController = require('../controllers/dashboard');
const bookController = require('../controllers/book');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post(
  '/createAdmin',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  adminValidator.adminValidators,
  adminController.createNewUser
);
router.post('/loginAdmin', adminController.loginAdmin);
router.post('/createSubject', adminController.createSubjects);
router.get('/getSubjects', adminController.getSubjects);

router.post('/createPattern', adminController.createPattern);
router.get('/getPattern', adminController.getPattern);
router.put(
  '/deleteUser',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  adminController.deleteUser
);
router.get(
  '/getUsers',
  tokenValidator.isAuth,
  isAdminValidator.isAdmin,
  adminValidator.adminValidators,
  adminController.getUsers
);
router.get(
  '/getDashboardData',
  tokenValidator.isAuth,
  isAdminValidator.isAdmin,
  dashboardController.getDashboard
);
router.post(
  '/createBook',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  bookController.createBook
);
router.get(
  '/getBooks',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  bookController.getBooks
);
router.put(
  '/deleteBook',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  bookController.deleteBook
);
router.get(
  '/getBookDetails',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  bookController.getBookDetails
);
router.put(
  '/editBook',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  bookController.editBook
);
exports.router = router;
