var express = require('express');
var router = express.Router();
const tokenValidator = require('../utils/token/validator');
const adminValidator = require('../utils/token/adminValidator');
const questionValidator = require('../middlewares/validators/questions');
const questionController = require('../controllers/questions');
const isAdminValidator = require('../utils/token/adminValidator');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post(
  '/createQuestion',
  tokenValidator.isAuth,
  adminValidator.isAdmin,
  questionValidator.questionValidators,
  questionController.createNew
);
router.put(
  '/editQuestion',
  tokenValidator.isAuth,
  adminValidator.isAdmin,
  questionValidator.questionValidators,
  questionController.edit
);
router.get(
  `/getQuestions`,
  tokenValidator.isAuth,
  adminValidator.isAdmin,
  questionController.getQuestions
);
router.put(
  '/deleteQuestion',
  tokenValidator.isAuth,
  adminValidator.isAdmin,
  questionController.deleteQuestion
);
router.put(
  '/approveQuestion',
  tokenValidator.isAuth,
  isAdminValidator.isMasterAdmin,
  questionController.approveQuestion
);
router.get('/getQuestionDetails', questionController.getQuestionDetails);
// router.get()

exports.router = router;
