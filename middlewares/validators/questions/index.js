const { body } = require('express-validator');
const Question = require('../../../models/questions');
exports.questionValidators = [
  body('title').trim().not().isEmpty().withMessage('Title Should Not Be Empty'),
  body('subject')
    .trim()
    .not()
    .isEmpty()
    .withMessage('Subject Should Not Be Empty'),
];
