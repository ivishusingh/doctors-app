const { body } = require('express-validator');
const User = require('../../../models/adminUsers');
exports.adminValidators = [
  body('userName').trim(),
  // .custom(async (value, { req }) => {
  //   try {
  //     const user = await User.findOne({ where: { email: value } });
  //     if (user) {
  //       return Promise.reject(
  //         'Email already exist. Please try with some other email!!!'
  //       );
  //     }
  //     return true;
  //   } catch (error) {
  //     const err = new Error(error.message);
  //     throw err;
  //   }
  // }),
  body('password')
    .trim()
    .not()
    .isEmpty()
    .isLength({ min: 8 })
    .withMessage('Please enter strong password atleast 8 characters'),
];
