const question = require('../../routes/question');
const adminUsers = require('../../routes/adminUsers');

exports.routeLoader = (app) => {
  app.use(question.router);
  app.use(adminUsers.router);
};
