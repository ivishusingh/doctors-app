import Sequelize from 'sequelize';

// Import models
const models = {
  adminUsers: Sequelize.import('./adminUsers'),
  Questions: Sequelize.import('./questions'),
};

Object.keys(models).forEach((modelKey) => {
  // Create model associations
  if ('associate' in models[modelKey]) {
    models[modelKey].associate(models);
  }
});
