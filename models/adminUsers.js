const Sequelize = require('sequelize');
const sequelize = require('../config/config');
const Questions = require('./questions');

const Admin_users = sequelize.define(
  'adminUsers',
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
    },
    firstName: { type: Sequelize.STRING },
    lastName: { type: Sequelize.STRING },
    mobileNo: { type: Sequelize.STRING },
    password: {
      type: Sequelize.STRING,
      required: true,
    },
    userType: { type: Sequelize.INTEGER },
    isActive: { type: Sequelize.BOOLEAN, default: true },
  },
  {
    timestamps: true,
  }
);

Admin_users.associate = function (models) {
  models.Admin_users.hasMany(models.Questions);
};

module.exports = Admin_users;
