const Sequelize = require('sequelize');
const sequelize = require('../config/config');

const Pattern = sequelize.define(
  'pattern',
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    label: {
      type: Sequelize.STRING,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = Pattern;
