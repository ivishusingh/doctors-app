const Sequelize = require('sequelize');
const sequelize = require('../config/config');
const adminUsers = require('./adminUsers');

const Book = sequelize.define(
  'book',
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    title: { type: Sequelize.TEXT, required: true },
    subject: { type: Sequelize.TEXT, required: true },
    edition: { type: Sequelize.TEXT },
    year: {
      type: Sequelize.STRING,
    },
    author: { type: Sequelize.TEXT },
    image: { type: Sequelize.STRING },

    isActive: { type: Sequelize.BOOLEAN, default: true },
    basicInfo: { type: Sequelize.JSONB },
    otherInfo: { type: Sequelize.JSONB },
    useFull: {
      type: Sequelize.JSONB,
    },
  },
  {
    timestamps: true,
  }
);

// Questions.belongsTo(adminUsers, { foreignKey: 'id', targetKey: 'id' });
module.exports = Book;
