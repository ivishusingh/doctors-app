const Sequelize = require('sequelize');
const sequelize = require('../config/config');

const Subjects = sequelize.define(
  'subjects',
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    label: {
      type: Sequelize.STRING,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = Subjects;
