const Sequelize = require('sequelize');
const sequelize = require('../config/config');
const adminUsers = require('./adminUsers');

const Questions = sequelize.define(
  'questions',
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    title: { type: Sequelize.TEXT, required: true },
    subject: { type: Sequelize.INTEGER, required: true },
    pattern: { type: Sequelize.INTEGER, required: true },
    answers: {
      type: Sequelize.TEXT,
    },
    referenceBook: { type: Sequelize.TEXT, required: true },
    image: { type: Sequelize.STRING },
    description: { type: Sequelize.TEXT },
    createdByUser: { type: Sequelize.INTEGER, required: true },
    isActive: { type: Sequelize.BOOLEAN, default: true },
    approved: { type: Sequelize.BOOLEAN, default: false },
    basicInfo: { type: Sequelize.JSONB },
    otherInfo: { type: Sequelize.JSONB },
    useFull: {
      type: Sequelize.JSONB,
    },
  },
  {
    timestamps: true,
  }
);

// Questions.belongsTo(adminUsers, { foreignKey: 'id', targetKey: 'id' });
module.exports = Questions;
