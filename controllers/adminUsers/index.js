const adminUserService = require('../../services/adminUsers');
const bcrypt = require('bcrypt');
const Subject = require('../../models/subjects');
const Pattern = require('../../models/pattern');
const adminUsers = require('../../models/adminUsers');
const { validationResult } = require('express-validator');
let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
exports.createNewUser = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    console.log(errors, 'ye dekh hhhhhhhhhhhhhhhhhhhh');
    if (!errors.isEmpty()) {
      console.log('i am inside');
      const error = new Error(`${errors.errors[0].msg}`);
      error.data = errors.array();
      error.statusCode = 422;
      throw error;
    }
    if (req.body.userName.match(mailformat)) {
      const hashPassword = await bcrypt.hash(req.body.password, 10);
      let data = {
        email: req.body.userName,
        password: hashPassword,
        userType: 2,
        lastName: req.body.lastName,
        firstName: req.body.firstName,
        mobileNo: req.body.mobileNumber,
        normalPassword: req.body.password,
      };
      console.log(data, 'here is data');
      let createUsers = await adminUserService.createNewUser(data);
      console.log(createUsers, 'this is');
      return res
        .status(createUsers.code)
        .send({ status: createUsers.code, message: createUsers.message });
    } else {
      return res.status(400).send({ message: 'Enter Valid Email', code: 400 });
    }
  } catch (err) {
    next(err);
  }
};
exports.loginAdmin = async (req, res, next) => {
  try {
    let data = {
      userName: req.body.userName.toLowerCase(),
      password: req.body.password,
    };
    if (data.userName && data.password && data.userName.match(mailformat)) {
      let loginUser = await adminUserService.loginAdmin(data);
      console.log(loginUser);
      return res.status(loginUser.code).send({
        status: loginUser.code,
        message: loginUser.message,
        data: loginUser.data,
      });
    } else {
      return res
        .status(400)
        .send({ status: 400, message: 'Enter Valid Credentials' });
    }
  } catch (err) {
    next(err);
  }
};

exports.createSubjects = async (req, res, next) => {
  try {
    let label = req.body.label;
    console.log(label);
    let newSubject = await new Subject({
      label,
    });
    let save = await newSubject.save();
    if (save) {
      return res.status(200).send({ message: 'saved', code: 200 });
    }
  } catch (err) {
    next(err);
  }
};
exports.getSubjects = async (req, res, next) => {
  try {
    let data = await Subject.findAll({
      order: [['label', 'ASC']],
    });

    if (data) {
      return res.status(200).send({ code: 200, data });
    }
  } catch (err) {
    next(err);
  }
};
exports.getUsers = async (req, res, next) => {
  try {
    let data = await adminUsers.findAndCountAll({
      offset: req.query.skip,
      limit: req.query.count,
      order: [['createdAt', 'DESC']],
    });
    if (data) {
      return res.status(200).send({ code: 200, data });
    }
  } catch (err) {
    next(err);
  }
};
exports.deleteUser = async (req, res, next) => {
  try {
    let id = req.query.id;
    let savedUser = await adminUsers.findOne({ where: { id: id } });
    if (savedUser) {
      await adminUsers.update(
        { isActive: false },
        { returning: true, where: { id: savedUser.id } }
      );
      return res.status(200).send({ message: 'deleted', status: 200 });
    }
  } catch (err) {
    next(err);
  }
};

exports.createPattern = async (req, res, next) => {
  try {
    let label = req.body.label;
    console.log(label);
    let newPattern = await new Pattern({
      label,
    });
    let save = await newPattern.save();
    if (save) {
      return res.status(200).send({ message: 'saved', code: 200 });
    }
  } catch (err) {
    next(err);
  }
};
exports.getPattern = async (req, res, next) => {
  try {
    let data = await Pattern.findAll({});

    if (data) {
      return res.status(200).send({ code: 200, data });
    }
  } catch (err) {
    next(err);
  }
};
