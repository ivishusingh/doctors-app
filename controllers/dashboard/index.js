const dashboardService = require('../../services/Dashboard');
exports.getDashboard = async (req, res, next) => {
  try {
    let result = await dashboardService.getDashboardData();
    return res
      .status(result.code)
      .send({ data: result.data, code: result.code });
  } catch (err) {
    next(err);
  }
};
