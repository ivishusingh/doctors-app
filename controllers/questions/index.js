const questionService = require('../../services/questions');
const { validationResult } = require('express-validator');
exports.createNew = async (req, res, next) => {
  try {
    // console.log(req.files.questionImage[0].location, 'here is image url');
    const errors = validationResult(req);
    console.log(req.body, 'body is here');
    if (!errors.isEmpty()) {
      const error = new Error(`${errors.errors[0].msg}`);
      error.data = errors.array();
      error.statusCode = 402;
      throw error;
    }
    let body = {
      title: req.body.title,
      subject: req.body.subject,
      pattern: req.body.pattern,
      answers: req.body.answers,
      referenceBook: req.body.referenceBook ? req.body.referenceBook : '',
      image: req.files.questionImage ? req.files.questionImage[0].key : null,
      description: req.body.description,
      createdByUser: req.user.id,
    };
    console.log(body);
    let result = await questionService.CreateNew(body);
    console.log(result, 'here is result');
    return res.status(result.code).send({ message: result.message });
  } catch (err) {
    next(err);
  }
};
exports.edit = async (req, res, next) => {
  try {
    // console.log(req.files.questionImage[0].location, 'here is image url');
    const errors = validationResult(req);
    console.log(req.body, 'body is here');
    if (!errors.isEmpty()) {
      const error = new Error(`${errors.errors[0].msg}`);
      error.data = errors.array();
      error.statusCode = 402;
      throw error;
    }
    let body = {
      id: req.query.id,
      title: req.body.title,
      subject: req.body.subject,
      pattern: req.body.pattern,
      answers: req.body.answers,
      referenceBook: req.body.referenceBook ? req.body.referenceBook : '',
      image: req.files.questionImage ? req.files.questionImage[0].key : null,
      description: req.body.description,
      createdByUser: req.user.id,
    };
    console.log(body);
    let result = await questionService.edit(body);
    console.log(result, 'here is result');
    return res.status(result.code).send({ message: result.message });
  } catch (err) {
    next(err);
  }
};

exports.deleteQuestion = async (req, res, next) => {
  try {
    let id = req.query.id;
    let result = await questionService.deletQuestion(id);
    console.log(result, 'here is result');
    return res
      .status(result.code)
      .send({ message: result.message, data: result.data });
  } catch (err) {
    next(err);
  }
};
exports.approveQuestion = async (req, res, next) => {
  try {
    let id = req.query.id;
    let result = await questionService.approveQuestion(id);
    return res
      .status(result.code)
      .send({ message: result.message, data: result.data });
  } catch (err) {
    next(err);
  }
};
exports.getQuestions = async (req, res, next) => {
  try {
    let data = {
      userType: req.user.userType,
      userId: req.user.id,
      skip: req.query.skip,
      count: req.query.count,
      subjectId: req.query.subjectId,
      pattern: req.query.pattern,
      verified: req.query.verified,
      searchPattern: req.query.searchPattern,
    };
    console.log(data, 'data');
    let result = await questionService.getQuestions(data);
    console.log(result, 'here is result');
    return res
      .status(result.code)
      .send({ message: result.message, data: result.data });
  } catch (err) {
    next(err);
  }
};
exports.getQuestionDetails = async (req, res, next) => {
  try {
    let data = {
      id: req.query.id,
    };
    console.log(req.query, 'here they are');
    let result = await questionService.getQuestionDetails(data);
    console.log(result, 'here is result');
    return res
      .status(result.code)
      .send({ message: result.message, data: result.data });
  } catch (err) {
    next(err);
  }
};
