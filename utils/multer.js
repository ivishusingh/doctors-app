var multer = require('multer');
const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const path = require('path');
const fileStorage = multer.diskStorage({
  destination: 'public/uploads',
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString() + '-' + file.originalname);
  },
});
// Medprep-rep
const s3 = new aws.S3({
  accessKeyId: 'AKIA22ZXCEBYW4E3PHNQ',
  secretAccessKey: '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',
  Bucket: 'medprep-rep',
});

exports.multer = async (app) => {
  app.use(
    multer({
      storage: multerS3({
        s3: s3,
        bucket: 'medprep-rep',
        acl: 'public-read',
        key: function (req, file, cb) {
          cb(
            null,
            path.basename(file.originalname, path.extname(file.originalname)) +
              new Date().toISOString() +
              ' - ' +
              path.extname(file.originalname)
          );
        },
        filename: (req, file, cb) => {
          console.log('inside file', file.Location);
          cb(null, new Date().toISOString() + '-' + file.originalname);
        },
      }),
    }).fields([
      {
        name: 'questionImage',
        maxCount: 1,
      },
      {
        name: 'bookImage',
        maxCount: 1,
      },
    ])
  );
};
