const jwtHandler = require('./jwt');
const adminUsers = require('../../models/adminUsers');

/**
 * @description JWT token validation
 * @param req
 * @param res
 * @param next
 * @returns success and error any encountered
 */
exports.isAuth = async (req, res, next) => {
  try {
    let authHeader = 'bearer' + ' ' + req.headers.authorization;
    console.log(authHeader, req.headers.authorization, 'authe');
    let decoded = jwtHandler.verifyJWT(res, authHeader);
    if (decoded) {
      req.user = decoded;
      let id = await adminUsers.findOne({ where: { id: req.user.id } });
      if (id) {
        console.log(req.user, 'from user');
        next();
      } else {
        return res
          .status(422)
          .send({ message: 'Session Expired please login', code: 422 });
      }
    }
  } catch (err) {
    next(err);
  }
};
