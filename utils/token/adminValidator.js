exports.isMasterAdmin = async (req, res, next) => {
  try {
    if (req.user.userType == 1) {
      console.log(req.user);
      next();
    } else {
      return res
        .status(400)
        .send({ message: 'you dont have permissions to do that', status: 400 });
    }
  } catch (err) {
    next(err);
  }
};
exports.isAdmin = async (req, res, next) => {
  try {
    if (req.user.userType == 1 || 2) {
      console.log(req.user);
      next();
    } else {
      return res
        .status(400)
        .send({ message: 'you dont have permissions to do that', status: 400 });
    }
  } catch (err) {
    next(err);
  }
};
