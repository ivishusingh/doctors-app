const adminUsers = require('../../models/adminUsers');
const adminService = require('../../services/adminUsers');
const bcrypt = require('bcrypt');
exports.createMasterAdmin = async (req, res, next) => {
  try {
    const constexistingAdmin = await adminUsers.findOne({
      where: { email: 'medprep@yopmail.com', userType: 1 },
    });
    if (constexistingAdmin) {
    } else {
      let password = 'Medicoprep1!';
      const hashPassword = await bcrypt.hash(password, 10);
      let data = {
        password: hashPassword,
        email: 'medprep@yopmail.com',
        firstName: 'Master',
        lastName: 'Admin',
        userType: 1,
      };
      let createUsers = await adminService.createNewUser(data);
      if (createUsers) {
        console.log('admin created');
      }
    }
  } catch (err) {
    return err;
  }
};
