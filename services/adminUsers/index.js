const adminsUsers = require('../../models/adminUsers');
const response = require('../../middlewares/response/index');
const bcrypt = require('bcrypt');
const jwtHandler = require('../../utils/token/jwt');
const sendMail = require('../../utils/sendEmail');
exports.createNewUser = async (data) => {
  try {
    const userExisting = await adminsUsers.findOne({
      where: { email: data.email },
    });
    if (!userExisting) {
      let newUser = await new adminsUsers({
        email: data.email,
        password: data.password,
        firstName: data.firstName,
        lastName: data.lastName,
        mobileNo: data.mobileNo,
        userType: data.userType,
        isActive: true,
      });

      await newUser.save();
      await sendMail.sendMail(data.email, data.normalPassword);
      let response = {
        code: 200,
        message: 'User Reactivated',
      };
      return response;
    } else if (userExisting && userExisting.isActive == false) {
      let Update = await adminsUsers.update(
        {
          email: data.email,
          password: data.password,
          firstName: data.firstName,
          lastName: data.lastName,
          mobileNo: data.mobileNo,
          userType: data.userType,
          isActive: true,
        },
        {
          where: { id: userExisting.id },
        }
      );
      await sendMail.sendMail(data.email, data.normalPassword);
      let response = {
        code: 200,
        message: 'User Reactivated',
      };
      return response;
    } else {
      let response = {
        code: 401,
        message: 'email already exist',
      };
      return response;
    }
  } catch (err) {
    return err;
  }
};
exports.loginAdmin = async (data) => {
  try {
    let response = {
      code: '',
      message: '',
      data: '',
    };
    let query = { email: data.userName, isActive: true };
    const savedUser = await adminsUsers.findOne({ where: query });
    if (savedUser) {
      let compare;

      compare = await bcrypt.compare(data.password, savedUser.password);
      console.log(
        'it is password',
        data.password,
        savedUser.password,
        savedUser.email
      );

      if (compare) {
        // console.log('from compared', compare);
        let token = jwtHandler.getJWT(
          savedUser.email,
          savedUser.userType,
          savedUser.id
        );
        let userDetails = {
          email: savedUser.email,
          userType: savedUser.userType,
          id: savedUser.id,
          token,
        };
        (response.code = 200),
          (response.message = `Login Succesfull`),
          (response.data = userDetails);
        return response;
      } else {
        (response.code = 400), (response.message = `Incorrect Password`);
        return response;
      }
    } else {
      (response.code = 400), (response.message = `User doesn't exist`);
      return response;
    }
  } catch (err) {
    (response.code = 402), (response.message = err);
    return response;
  }
};
