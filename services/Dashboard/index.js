const Questions = require('../../models/questions');
exports.getDashboardData = async () => {
  try {
    let data = {};

    //subject wise
    data.totalQuestions = await Questions.count({ where: { isActive: true } });
    data.PhysiologyCount = await Questions.count({
      where: { subject: 1, isActive: true },
    });
    data.BiochemistryCount = await Questions.count({
      where: { subject: 2, isActive: true },
    });
    data.AnatomyCount = await Questions.count({
      where: { subject: 3, isActive: true },
    });
    data.MicrobiologyCount = await Questions.count({
      where: { subject: 4, isActive: true },
    });
    data.PathologyCount = await Questions.count({
      where: { subject: 5, isActive: true },
    });
    data.ForensicMedicineCount = await Questions.count({
      where: { subject: 6, isActive: true },
    });
    data.EyeCount = await Questions.count({
      where: { subject: 7, isActive: true },
    });
    data.EntCount = await Questions.count({
      where: { subject: 8, isActive: true },
    });
    data.MedicineCount = await Questions.count({
      where: { subject: 9, isActive: true },
    });
    data.SurgeryCount = await Questions.count({
      where: { subject: 10, isActive: true },
    });
    data.OthopaedicsCount = await Questions.count({
      where: { subject: 11, isActive: true },
    });
    data.DermatologyCount = await Questions.count({
      where: { subject: 12, isActive: true },
    });
    data.PharmacologyCount = await Questions.count({
      where: { subject: 13, isActive: true },
    });
    data.preventtiveMedicine = await Questions.count({
      where: { subject: 14, isActive: true },
    });
    data.obGYn = await Questions.count({
      where: { subject: 15, isActive: true },
    });

    data.Paediatrics = await Questions.count({
      where: { subject: 18, isActive: true },
    });
    data.radioDiagnosis = await Questions.count({
      where: { subject: 19, isActive: true },
    });
    data.Psychiatry = await Questions.count({
      where: { subject: 20, isActive: true },
    });
    data.Anaesthesia = await Questions.count({
      where: { subject: 21, isActive: true },
    });

    // pattern wise
    data.Neet = await Questions.count({
      where: { pattern: 1, isActive: true },
    });
    data.Aiims = await Questions.count({
      where: { pattern: 2, isActive: true },
    });
    data.Bhu = await Questions.count({
      where: { pattern: 3, isActive: true },
    });
    data.Pgi = await Questions.count({
      where: { pattern: 4, isActive: true },
    });
    data.Jipmer = await Questions.count({
      where: { pattern: 5, isActive: true },
    });
    let response = {
      data,
      code: 200,
    };
    return response;
  } catch (err) {
    throw new Error(err);
  }
};
