const Question = require('../../models/questions');
const aws = require('aws-sdk');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const s3 = new aws.S3({
  accessKeyId: 'AKIA22ZXCEBYW4E3PHNQ',
  secretAccessKey: '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',
  Bucket: 'medprep-rep',
});
module.exports = {
  async CreateNew(body) {
    console.log(body, 'from inside');

    let saveData = new Question({
      title: body.title,
      subject: body.subject,
      pattern: body.pattern,
      answers: body.answers,
      referenceBook: body.referenceBook,
      image: body.image,
      isActive: true,
      description: body.description,
      createdByUser: body.createdByUser,
      approved: false,
    });
    let question = await saveData.save();
    if (saveData) {
      let response = {
        code: 200,
        message: 'Created',
      };
      return response;
    }
  },
  async edit(data) {
    let id = parseInt(data.id);
    let savedQuestion = await Question.findOne({ where: { id } });

    if (data.image) {
      var deleteParam = {
        Bucket: 'medprep-rep',
        Delete: {
          Objects: [{ Key: savedQuestion.image }],
        },
      };
      s3.deleteObjects(deleteParam, function (err, data) {
        if (err) console.log(err, err.stack);
        else console.log('delete', data);
      });
    }

    console.log(savedQuestion.id, id, 'from insside');
    let saveData = await Question.update(
      {
        title: data.title ? data.title : savedQuestion.title,
        subject: data.subject ? data.subject : savedQuestion.subject,
        pattern: data.pattern,
        answers: data.answers,
        pageNo: data.pageNo ? data.pageNo : savedQuestion.pageNo,
        referenceBook: data.referenceBook
          ? data.referenceBook
          : savedQuestion.referenceBook,
        image: data.image ? data.image : savedQuestion.image,
        isActive: true,
        description: data.description
          ? data.description
          : savedQuestion.description,
        createdByUser: data.createdByUser,
        approved: false,
      },
      { where: { id } }
    );
    if (saveData) {
      let response = {
        code: 200,
        message: 'Created',
      };
      return response;
    }
  },
  async getQuestions(userData) {
    try {
      let response = {
        code: '',
        message: '',
      };
      let data;
      let query;
      if (userData.userType === 1) {
        query = {
          isActive: true,
          subject: userData.subjectId ? userData.subjectId : { [Op.ne]: null },
          title: { [Op.like]: '%' + userData.pattern + '%' },
          approved: userData.verified ? userData.verified : { [Op.ne]: null },
        };
        data = await Question.findAndCountAll({
          where: query,
          offset: userData.skip,
          limit: userData.count,
          order: [['createdAt', 'DESC']],
        });
      } else {
        query = {
          isActive: true,
          createdByUser: userData.userId,
          subject: userData.subjectId ? userData.subjectId : { [Op.ne]: null },
          title: { [Op.like]: '%' + userData.pattern + '%' },
          approved: userData.verified ? userData.verified : { [Op.ne]: null },
        };
        data = await Question.findAndCountAll({
          where: query,
          offset: userData.skip,
          limit: userData.count,
          order: [['createdAt', 'DESC']],
        });
      }

      (response.code = 200),
        (response.message = 'success'),
        (response.data = data);
      return response;
    } catch (err) {
      throw new Error(err);
    }
  },
  async deletQuestion(id) {
    try {
      let response = {
        code: '',
        message: '',
      };
      let question = await Question.findOne({ where: { id } });
      if (question) {
        const Update = await Question.update(
          { isActive: false },
          { returning: true, where: { id: question.id } }
        );

        (response.code = 200), (response.message = 'Deleted');
        return response;
      }
    } catch (err) {
      throw new Error(err);
    }
  },
  async approveQuestion(Id) {
    try {
      let response = {
        code: '',
        message: '',
      };
      let questions = await Question.findOne({ where: { id: Id } });
      if (questions) {
        const Update = await Question.update(
          { approved: true },
          { returning: true, where: { id: questions.id } }
        );
        console.log('approved');
        (response.code = 200), (response.message = 'Approved');
        return response;
      }
    } catch (err) {
      throw new Error(err);
    }
  },
  async getQuestionDetails(datas) {
    try {
      let response = {
        code: '',
        message: '',
        data: '',
      };
      let id = datas.id;
      let data = await Question.findOne({ where: { id } });
      (response.code = 200),
        (response.message = 'details'),
        (response.data = data);
      return response;
    } catch (err) {
      throw new Error(err);
    }
  },
};
