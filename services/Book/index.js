const Book = require('../../models/book');
const aws = require('aws-sdk');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const s3 = new aws.S3({
  accessKeyId: 'AKIA22ZXCEBYW4E3PHNQ',
  secretAccessKey: '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',
  Bucket: 'medprep-rep',
});
exports.createNew = async (body) => {
  try {
    let newBook = new Book({
      title: body.bookTitle,
      subject: body.subject,
      edition: body.edition,
      year: body.year,
      image: body.bookImage,
      author: body.author,
      isActive: true,
    });
    let save = await newBook.save();
    let response = {
      code: 200,
      message: 'added',
    };
    return response;
  } catch (err) {
    throw new Error(err);
  }
};
exports.getBooks = async (body) => {
  try {
    let data = await Book.findAndCountAll({
      offset: body.offset,
      limit: body.limit,
      order: [['createdAt', 'DESC']],
    });
    let response = {
      code: 200,
      message: 'list',
      data,
    };
    return response;
  } catch (err) {
    throw new Error(err);
  }
};
exports.getBookDetails = async (id) => {
  try {
    let data = await Book.findOne({ where: { id } });
    let response = {
      code: 200,
      message: 'Details',
      data,
    };
    return response;
  } catch (err) {
    throw new Error(err);
  }
};
exports.deleteBook = async (id) => {
  try {
    let savedbook = await Book.findOne({ where: { id } });
    if (savedbook) {
      await Book.update(
        { isActive: false },
        { returning: true, where: { id: savedbook.id } }
      );
      console.log(savedbook, 'from service');
      let response = {
        code: 200,
        message: 'Deleted',
      };
      return response;
    }
  } catch (err) {
    throw new Error(err);
  }
};
exports.editBook = async (body) => {
  try {
    let id = parseInt(body.id);
    let savedBook = await Book.findOne({ where: { id } });
    if (body.image) {
      var deleteParam = {
        Bucket: 'medprep-rep',
        Delete: {
          Objects: [{ Key: savedBook.image }],
        },
      };
      s3.deleteObjects(deleteParam, function (err, data) {
        if (err) console.log(err, err.stack);
        else console.log('delete', data);
      });
    }
    let saveData = await Book.update(
      {
        title: body.bookTitle,
        subject: body.subject,
        edition: body.edition,
        year: body.year,
        image: body.bookImage,
        author: body.author,
      },
      { where: { id } }
    );
    if (saveData) {
      let response = {
        code: 200,
        message: 'Created',
      };
      return response;
    }
  } catch (err) {
    throw new Error(err);
  }
};
