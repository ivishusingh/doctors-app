var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');
const routeMiddleware = require('./middlewares/routes/index');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./doctor-app.yaml');
var app = express();
var AWS = require('aws-sdk');
const sequelize = require('./config/config');
const multer = require('./utils/multer');
multer.multer(app);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));
routeMiddleware.routeLoader(app);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

//swagger middleware
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
sequelize
  .sync()
  .then((data) => {
    console.log('table created successfully');
  })
  .then(() => {
    const createAdmin = require('./utils/seeder/masterAdmin').createMasterAdmin;
    createAdmin();
  })
  .catch((err) => {
    console.log(err);
    // next(err);
  });

// error handler
app.use((error, req, res, next) => {
  console.log('inside error middleware()');
  console.log(error);

  let statusCode;

  if (
    error.name === 'ValidationError' ||
    error.name === 'MongoError' ||
    error.name === 'CastError'
  ) {
    if (error.message !== 'pool destroyed') {
      error.statusCode = 402;
    }
    if (error.name === 'CastError') {
      error.message = `invalid ${error.path}`;
    }
  }
  if (error.name === 'MulterError') {
    error.statusCode = 402;
    error.message = `${error.message}: ${error.field}`;
  }

  if (!error.statusCode) {
    error.statusCode = 500;
  }
  statusCode = error.statusCode;
  console.log(statusCode, error.message);
  res.status(statusCode).json({ status: statusCode, message: error.message });
});

module.exports = app;
